<?php
/**
 * Template Name: Tag
 *
 * @package WordPress
 */
get_header(); ?> 
	<!-- based from default template page.php -->
	<main id="primary" class="site-main">
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		
		<div class="entry-content">
			<?php $tag = $post->post_name; // value for tag to be passed in query
			$posts_per_page = $post->posts_per_page; ?>
			<div id="posts">
				<?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') :1;	// get first set of queried posts
				$args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'tag' => $tag,	// post_name is slug of page
					'posts_per_page' => $posts_per_page,
					'paged' => $paged	// page number to the set of posts by posts_per_page
				);
				query_posts( $args );
				 
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', get_post_type());	// get content.php to show posts
					endwhile;
				else :

					get_template_part( 'template-parts/content', 'none' );	// get content-none.php if no posts available
					
				endif;
				wp_reset_postdata();
				?>
			</div>
			<?php wp_reset_query(); // reset query to get content from page editor ?>
			<!--<button id="loadmoretag">Load More</button>-->
			<?php the_content();	// access shortcode in page editor ?>
		</div><!-- .entry-content -->
	</main><!-- #main -->
	
<?php
get_sidebar();
get_footer();
