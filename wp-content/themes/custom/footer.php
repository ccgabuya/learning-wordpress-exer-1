<?php
/**
 * The template for displaying the footer
 *
 * @package custom
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'custom' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'custom' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'custom' ), 'custom', '<a href="http://localhost">Christine Gabuya</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
$(document).ready(function() {
	<?php $paged = ( get_query_var('paged')) ? get_query_var('paged') :1; ?>
	page = <?php echo $paged; ?>;	// value of current page to be passed to function
	posts_per_page = <?php echo $posts_per_page; ?>;	// posts per page
	$("#loadmoretag").click(function() {
		$.ajax({
			url: '/wordpress/wp-admin/admin-ajax.php',	//url of admin-ajax
			type: 'post',	// data to be passed as $_POST
			data: {
				'action': 'loadmoretag',	// call in functions.php
				'page': page,	// pass value of current page
				'posts_per_page': posts_per_page,	// pass value posts per page
				'post_name': '<?php echo $post->post_name; ?>'	// pass page slug for tag value
			},
			success: function(posts) {
				$('#posts').append(posts);	// if success, append posts in <div id="posts">
				page++;	// increment page number for next set of posts
			}
		});
	});
})
$(document).ready(function() {
	<?php $paged = ( get_query_var('paged')) ? get_query_var('paged') :1; ?>
	page = <?php echo $paged; ?>;	// value of current page to be passed to function
	posts_per_page = <?php echo $posts_per_page; ?>;	// posts per page
	$("#loadmorecategory").click(function() {
		$.ajax({
			url: '/wordpress/wp-admin/admin-ajax.php',	// url of admin-ajax for wordpress
			type: 'post',	// data to be passed as $_POST
			data: {
				'action': 'loadmorecategory',	// call in functions.php
				'page': page,	// pass value of current page
				'posts_per_page': posts_per_page,	// pass value posts per page
				'post_name': '<?php echo $post->post_name; ?>'	// pass page slug for category value
			},
			success: function(posts) {
				$('#posts').append(posts);	// if success, append posts in <div id="posts">
				page++; // increment page number for next set of posts
			}
		});
	});
})
</script>
</html>
