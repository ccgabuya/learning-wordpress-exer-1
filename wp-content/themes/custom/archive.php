<?php
/**
 * The template for displaying archive pages
 *
 * @package custom
 */

get_header();
?>
	<div class="container">
		<div id="primary" class="content-area">
			<main id="primary" class="site-main">

			<?php if ( have_posts() ) : ?>
			
				<header class="page-header">
					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
					?>
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>

			</main><!-- #main -->
		</div> <!-- .content-area -->
		
		<div class="container-widget">
			<?php get_sidebar(); ?>
		</div><!-- .container.widget -->
	</div> <!-- .container -->
<?php
get_footer();
