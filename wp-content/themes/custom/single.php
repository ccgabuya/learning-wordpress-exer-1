<?php
/**
 * The template for displaying all single posts
 *
 * @package custom
 */

get_header();
?>

	<div class="container-page">
		<div class="page-area">
			<main id="primary" class="site-main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					the_post_navigation(
						array(
							'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'custom' ) . '</span> <span class="nav-title">%title</span>',
							'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'custom' ) . '</span> <span class="nav-title">%title</span>',
						)
					);

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
			
		</div> <!-- .page-area -->
		<div class="container-widget">
			<?php get_sidebar('single'); // get single-sidebar.php instead of sidebar.php ?>
		</div><!-- .container.widget -->
	</div> <!-- .container-page -->

<?php
get_footer();
