<?php
/**
 * The main template file
 * 
 * @package custom
 */

get_header();
?>
	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">

			<?php
			if ( have_posts() ) {

				// Load posts loop.
				while ( have_posts() ) {
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );
				}

				// Previous/next page navigation.
				the_posts_navigation();

			} else {

				// If no content, include the "No posts found" template.
				get_template_part( 'template-parts/content/content', 'none' );

			}
			?>

			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="container-widget">
			<?php get_sidebar(); ?>
		</div><!-- .container.widget -->
	</div> <!-- .container -->

<?php
get_footer();
