<?php
/**
 * The sidebar for single posts
 *
 * @package custom
 */
 
if ( ! is_active_sidebar( 'sidebar-single' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-single' ); ?>
	<div class="related-posts-after-content">	
	<h2>Related Posts:</h2>	
	<?php
		$tags = get_the_tags($post->ID);	// gets tags from post using $post->ID
		$categories = get_the_category($post->ID);	// gets categories from post using $post->ID
		if($tags || $categories) {	// get related posts if post has tags or categories
			$tag_array = array();	// create array for tags especially if multiple tags
			$cat_array = array();	// create array for categories especially if multiple categories
			if($tags) { // get tags if post has tags
				foreach($tags as $i => $tag) {
					$tag_array[$i] = $tag->slug;	// assign one tag for each array element
			} 	}
			if($categories) {	// get categories if post has categories
				foreach($categories as $j => $category) {
					$cat_array[$j] = $category->slug;	// assign one category for each array element
			}	}
			$args=array(
				'post__not_in' => array($post->ID),	// exclude the post from related posts
				'posts_per_page' => 5,	// number of related posts to load
				'tax_query' => array (	// array of post taxonomies to be used for query
					'relation' => 'OR',
					array(
						'taxonomy' => 'post_tag',	// get post using tag taxonomy
						'field' => 'slug',	// select within slugs of post
						'terms' => $tag_array	// terms to be used are tags of post
					),
					array(
						'taxonomy' => 'category',	// get post using category taxonomy
						'field' => 'slug',	// select within slugs of post
						'terms' => $cat_array	// terms to be used are categories of post
					)
				)
			);
			$query = new wp_query( $args );	
			while( $query->have_posts() ) {
				$query->the_post();	?>	
				<div>	
					<a href="<?php the_permalink();/* link to related post */?>">	
					<br />
					<?php the_title(); /* show permalink as title of post */ ?>	</a>		
				</div>
	<?php 	}
		} wp_reset_postdata(); ?>	

	</div>
</aside><!-- #secondary -->