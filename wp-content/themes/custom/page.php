<?php
/**
 * The template for displaying all pages
 *
 * @package custom
 */

get_header();
?>
	<div class="container-page">
		<div class="page-area">
			<main id="primary" class="site-main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile;
				?>

			</main><!-- #main -->
		</div> <!-- .page-area -->
		<div class="container-widget">
			<?php get_sidebar(); ?>
		</div><!-- .container.widget -->
	</div> <!-- .container-page -->

<?php
get_footer();
