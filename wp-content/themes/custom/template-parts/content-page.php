<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom
 */

?>

<article id="post-<?php the_ID(); ?>" class="post-page">
	<header class="post-header">
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
	</header><!-- .post-header -->

	<div class="post-content">
		<?php
		custom_post_thumbnail(); 
		
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'custom' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .post-content -->

	<footer class="post-footer">
		<?php custom_entry_footer(); ?>
	</footer><!-- .post-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
