<?php
/**
 * Template part for displaying posts
 *
 * @package custom
 */

?>

<article id="post-<?php the_ID(); ?>" class="content">
	
	<?php custom_post_thumbnail(); ?>
	
	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'custom' ),
					array(
						'span' => array(
						'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'custom' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h2 class="entry-title">', '</h2>' );
		else :
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		endif;?>
	</header><!-- .entry-header -->


	<footer class="entry-footer">
		<?php custom_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
